﻿using BaleLib;
using BaleLib.Models;
using BaleLib.Models.Parameters;
using BaleLib.Models.Updates;
using System;
using System.Collections.Generic;
using System.Threading;

namespace BaleEcho
{
    class Program
    {
        static BaleClient _client = new BaleClient("c8cf917a4ef28945c20f--------------");
        static void Main(string[] args)
        {
            int offset = 0;
            while (true)
            {
                Response<List<Update>> updates = _client.GetUpdatesAsync(offset).Result;

                if (updates.Ok && updates.Result.Count > 0)
                    foreach (Update update in updates.Result)
                    {
                        offset = (int)update.UpdateId + 1;
                        long chatId = update.Message.Chat.Id;

                        // image
                        if (update.Message.Photo != null)
                        {
                            ReplyToPhotoMessage(update, chatId);
                        }
                        // text message 
                        else if (!string.IsNullOrEmpty(update.Message.Text))
                        {
                            ReplyToTextMessage(update);
                        }
                    }

                Thread.Sleep(500);
            }
        }

        private static void ReplyToPhotoMessage(Update update, long chatId)
        {
            // get first item from gallery/array
            Photo photo = update.Message.Photo[0];
            string fileName = photo.FileId.Replace(":", "") + ".jpg";
            string filePath = "S:\\baleDownload\\";

            // download and save file
            if (_client.DownloadFile(chatId, photo.FileId, filePath, fileName))
            {

                // upload photo
                Response uploadResponse = _client.SendPhotoAsync(new PhotoMessage()
                {
                    ChatId = chatId,
                    Photo = Utils.ToBytes(filePath + fileName),
                    Caption = "echo"
                }).Result;

                Console.WriteLine("Photo upload, Success: " + uploadResponse.Ok.ToString());
            }
        }

        private static void ReplyToTextMessage(Update update)
        {
            Response response = _client.SendTextAsync(new TextMessage()
            {
                ChatId = update.Message.Chat.Id,
                Text = update.Message.Text
            }).Result;

            Console.WriteLine("Send text, Success: " + response.Ok.ToString());
        }
    }
}
